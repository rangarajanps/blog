class Comment < ActiveRecord::Base
    belongs_to :post
    validates :body, presence: true
    validates_presence_of :post_id
    validates_associated :post
end